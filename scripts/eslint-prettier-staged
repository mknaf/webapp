#!/bin/bash

# Taken from https://gist.github.com/luuuis/e41fd71134ce88ac5e9359cbdbfb6273
# This script is used by husky to run eslint only on the *staged* changes
# instead of *all* changes in the repository.

set -uo pipefail
IFS=$'\n\t'

#
# Improvements from dahjelle/pre-commit.sh:
# - does not lint deleted files,
# - lints all staged files before exiting with an error code,
# - handles spaces and other unusual chars in file names.
#
# Based also on @jancimajek's one liner in that Gist.
#

# ESLint staged changes only
git diff --diff-filter=d --cached --name-only -z -- 'webapp/**/*.js' \
  | xargs -0 -I % sh -c 'git show ":%" | npx eslint --stdin --stdin-filename "%";'
eslint_exit=$?

if [ ${eslint_exit} -eq 0 ]; then
  echo "✓ ESLint/Prettier passed."
else
  echo "✘ ESLint/Prettier failed." 1>&2
fi

exit ${eslint_exit}
